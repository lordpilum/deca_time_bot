use chrono::{Utc, NaiveDate, DateTime};
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MastoNotificationEvent
{
	pub id: String,
	pub r#type: String,
	pub created_at: DateTime<Utc>,
	pub account: Account,
	pub status: Option<Status>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Status
{
	pub id: String,
	pub created_at: Option<DateTime<Utc>>,
	pub in_reply_to_id: Option<String>,
	pub in_reply_to_account_id: Option<String>,
	pub sensitive: bool,
	pub spoiler_text: Option<String>,
	pub visibility: String,
	pub language: Option<String>,
	pub uri: Option<String>,
	pub url: Option<String>,
	pub replies_count: Option<u32>,
	pub reblogs_count: Option<u32>,
	pub favourites_count: Option<u32>,
	pub favourited: bool,
	pub reblogged: bool,
	pub muted: bool,
	pub bookmarked: bool,
	pub content: Option<String>,
	pub reblog: Option<String>,
	pub account: Option<Account>,
	pub media_attachments: Option<Vec<MediaAttachment>>,
	pub mentions: Option<Vec<Mention>>,
	pub tags: Option<Vec<Tag>>,
	pub emojis: Option<Vec<Emoji>>,
	pub card: Option<Card>,
	pub poll: Option<Poll>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Application
{
	pub name: String,
	pub website: Option<String>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Account
{
	pub id: String,
    pub username: String,
    pub acct: String,
    pub display_name: String,
    pub locked: bool,
    pub bot: bool,
    pub discoverable: bool,
    pub group: bool,
    pub created_at: DateTime<Utc>,
    pub note: Option<String>,
    pub url: String,
    pub avatar: String,
    pub avatar_static: String,
    pub header: String,
    pub header_static: String,
    pub followers_count: u32,
    pub following_count: u32,
    pub statuses_count: u32,
    pub last_status_at: NaiveDate,
    pub emojis: Option<Vec<Emoji>>,
    pub fields: Option<Vec<Field>>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Emoji
{
	pub shortcode: String,
	pub url: String,
	pub static_url: String,
	pub visible_in_picker: bool
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Field
{
	pub name: String,
	pub value: String,
	pub verified_at: Option<DateTime<Utc>>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Mention
{
    pub id: String,
    pub username: String,
    pub url: String,
    pub acct: String
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Card
{
	pub name: String,
	pub value: String,
	pub verified_at: Option<DateTime<Utc>>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Poll
{
	pub id: u64,
	pub expires_at: DateTime<Utc>,
	pub expired: bool,
	pub multiple: bool,
	pub votes_count: u32,
	pub voters_count: Option<u32>,
	pub voted: bool,
	pub own_votes: Option<Vec<u32>>,
	pub options: Vec<PollOption>,
	pub emojis: Option<Vec<String>>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PollOption
{
	pub title: String,
	pub votes_count: u32
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MediaAttachment
{
	pub id: String,
	pub r#type: String,
	pub url: String,
	pub preview_url: Option<String>,
	pub remote_url: Option<String>,
	pub text_url: Option<String>,
	pub meta: Option<Meta>,
	pub description: Option<String>,
	pub blurhash: Option<String>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Meta
{
	pub original: MediaSize,
	pub small: MediaSize,
	pub focus: Option<Focus>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MediaSize
{
	pub width: u32,
	pub height: u32,
	pub size: String,
	pub aspect: f32
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Focus
{
	pub x: f32,
	pub y: f32
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Tag
{
	pub name: String,
	pub url: Option<String>,
	pub history: Option<Vec<TagHistory>>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TagHistory
{
	pub day: String,
	pub uses: String,
	pub accounts: String
}
