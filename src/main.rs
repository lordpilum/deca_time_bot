mod masto;
mod dit;

use std::env;
use chrono::prelude::*;
use reqwest::header::*;
use tokio::time::{interval, interval_at, Duration, Instant};

use masto::MastoNotificationEvent;

#[tokio::main]
async fn main()
{
	let (_poster_status, _listener_status) = tokio::join!(
		run_poster(),
		run_listener()
	);
}

async fn run_poster()
{
	let start = calculate_start_time();
	let mut interval = interval_at(start, Duration::from_millis(2160000));

	println!("DIT time posting service running!");

	loop
	{
		interval.tick().await;

		let time = dit::get_deca_time();
		let stardit = dit::get_stardit();

		println!("Posting time at {:#?}", time);

		post_time(time, stardit).await
	}
}

async fn run_listener()
{
	let mut interval = interval(Duration::from_millis(5000));

	println!("Notification poller running!");

	loop
	{
		interval.tick().await;

		reply_to_notifications().await
	}
}

// The application should post every 25th demin,
// and the interval between each 25 demin is 2 160 000 milliseconds.
// Therefore we need to calculate the next 25th demin based on the current time offset.
fn get_next_run_time(offset: u64) -> u64
{
	let step = 2160000;
	let mut current = 0;

	while current <= offset
	{
		current += step;
	}

	current
}

fn get_client<U: reqwest::IntoUrl>(method: reqwest::Method, url: U) -> reqwest::RequestBuilder
{
	let token = env::var("DECA_CLIENT_TOKEN").expect("Client token must be set.");

	reqwest::Client::new()
		.request(method, url)
		.header(AUTHORIZATION, format!("Bearer {}", token))
		.header(CONTENT_TYPE, "application/json")
		.header(ACCEPT, "application/json")
}

fn calculate_start_time() -> Instant
{
	let base_timezone = FixedOffset::west_opt(12 * 3600).unwrap();
	let time_now = Utc::now().with_timezone(&base_timezone);
	let midnight = base_timezone.with_ymd_and_hms(time_now.year(), time_now.month(), time_now.day(), 0, 0, 0).unwrap();

	let offset = (time_now.timestamp_millis() - midnight.timestamp_millis()) as u64;
	let next_run_time = get_next_run_time(offset);
	let time_to_next_run = next_run_time - offset;

	Instant::now() + Duration::from_millis(time_to_next_run)
}

async fn post_time(time: f64, stardit: Vec<u32>)
{
	let year = stardit[0];
	let day = stardit[1];

	let status = format!("Current StarDIT (Decimal Internet Time)\r\nYear {year}\r\nDay {day}\r\nTime {time:.2}\r\n\r\n\r\n{year}:{day}:{time:.2}", year=year, day=day, time=time);

	let parameters = [
		("status", status),
		("visibility", String::from("public"))
	];

	let _response = get_client(reqwest::Method::POST, "https://fedi.j1nk4l.com/api/v1/statuses")
		.form(&parameters)
		.send()
		.await
		.expect("Panic!");
}

async fn reply_to_notifications()
{
	let events = get_notifications().await;

	for event in events.into_iter()
	{
		let log_event = event.clone();
		let url = log_event.status.unwrap().url.unwrap();
		println!("Replying to notification {} from {}.", log_event.id, url);

		reply_with_time(event.clone()).await;
		clear_notification(event).await;
	}
}

async fn get_notifications() -> Vec<masto::MastoNotificationEvent>
{
	let parameters = [
		("exclude_types[]", "follow"),
		("exclude_types[]", "favourite"),
		("exclude_types[]", "reblog"),
		("exclude_types[]", "poll"),
		("exclude_types[]", "follow_request"),
	];

	let response = get_client(reqwest::Method::GET, "https://fedi.j1nk4l.com/api/v1/notifications/")
		.query(&parameters)
		.send()
		.await;

	let result = match response {
		Ok(result) => result,
		Err(_) =>
		{
			println!("Request error.");
			return vec![];
		}
	};

	let body = result.text().await.expect("Failed to get response body."); // Todo: handle err
	let time = dit::get_deca_time();

	// If we're not getting an array, that means we're getting some kind of server error.
	// We're not going to attempt to deserialize it.
	if body.starts_with("{")
	{
		println!("Failed to deserialize statuses {:#?}", body);
		return Vec::new();
	}

	let statuses : Vec<MastoNotificationEvent> = serde_json::from_str(&body).unwrap_or_default();

	if body != "[]"
	{
		println!("Notifications received at {:#?}", time);
	}

	statuses
}

async fn clear_notification(event: MastoNotificationEvent)
{
	let _response = get_client(reqwest::Method::POST, format!("https://fedi.j1nk4l.com/api/v1/notifications/{}/dismiss", event.id))
		.send()
		.await
		.expect("Panic!");
}

async fn reply_with_time(event: MastoNotificationEvent)
{
	let time = dit::format_desec(dit::get_deca_time());

	let stardit = dit::get_stardit();
	let year = stardit[0];
	let day = stardit[1];

	let status = event.status.expect("Unable to get status.");
	let status_id = status.id;
	let visibility = status.visibility;
	let mentioner = event.account.acct;

	let parameters = [
		("status", format!("@{} the current year, day, and time is {}:{}:{} DIT.", mentioner, year, day, time)),
		("visibility", format!("{}", visibility)),
		("in_reply_to_id", format!("{}", status_id))
	];

	let _response = get_client(reqwest::Method::POST, "https://fedi.j1nk4l.com/api/v1/statuses")
		.form(&parameters)
		.send()
		.await
		.expect("Panic!");
}
