use chrono::prelude::*;

pub fn get_deca_time() -> f64
{
	// Deca time is based on UTC -12:00.
	let base_timezone = FixedOffset::west_opt(12 * 3600).unwrap();
	let time_now = Utc::now().with_timezone(&base_timezone);

	// We need to get midnight of the day in question in order to calculate the offset.
	let midnight = base_timezone.with_ymd_and_hms(time_now.year(), time_now.month(), time_now.day(), 0, 0, 0).unwrap();

	// Calculating the offset lets us turn the time into a decimal fraction of the day total.
	// Getting the time in milliseconds is probably a bit extravagant, but a desec *is* slightly
	// shorter than a regular second. Let the consumer handle the desired formatting.
	let offset_in_ms = time_now.timestamp_millis() - midnight.timestamp_millis();

	// There are 86 400 seconds in a day (well, not technically, but we're sticking to the regular
	// way of handling time for no), so therefore there are 86 400 000 milliseconds.
	// We're dividing a day into 10, so dividing by 8 640 000 yields us the desired result.
	offset_in_ms as f64 / 8640000.0
}

pub fn get_stardit() -> Vec<u32>
{
	// Deca time is based on UTC -12:00.
	let base_timezone = FixedOffset::west_opt(12 * 3600).unwrap();
	let time_now = Utc::now().with_timezone(&base_timezone);

	// Get year and day values.
	let dit_year = time_now.year() as u32 + 10000;
	let dit_day = time_now.ordinal();

	vec![ dit_year, dit_day ]
}

pub fn format_desec(time: f64) -> String
{
	let formatted = format!("{:.4}", time);

	let sections: Vec<&str> = formatted.split('.').collect();

	let deca = sections[0];
	let demin = &sections[1][..2];
	let desec = &sections[1][2..];

	format!("{}.{}.{}", deca, demin, desec)
}
