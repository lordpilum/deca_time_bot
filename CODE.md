# Forking

Go ahead. You can do whatever you want as long as it falls within the terms of the licence.

# Contributing

If you want to submit pull requests to this project, keep in mind the following code conventions:

* Indentation is a single tabulator per level.
* Curly braces start on a new line, at the current level of indentation.

I know these aren't according to common practices in Rust, but those are my terms.
I will reject any pull request outright if they do not follow these simple guidelines.
