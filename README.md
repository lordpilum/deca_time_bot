# `Deca Time Bot`

Do you want to run your own Mastodon Decimal Internet Time (DIT, Deca)? Say no more.

## About

`Deca Time Bot` is a bot which posts the current DIT every set interval. By default, every 25 demin.

## References
[http://djeekay.net/dit/](http://djeekay.net/dit/)
